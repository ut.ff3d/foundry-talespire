console.log("Hello World! This code runs immediately when the file is loaded.");

Hooks.on("init", function() {
  console.log("This code runs once the Foundry VTT software begins it's initialization workflow.");
});

Hooks.on("ready", function() {
  console.log("This code runs once core initialization is ready and game data is available.");
});


/**
 * Intercepts all roll-type messages hiding the content until the animation is finished
 */
 Hooks.on('createChatMessage', (chatMessage) => {
    let hasInlineRoll = chatMessage.data.content.indexOf('inline-roll') !== -1;
    if ((!chatMessage.isRoll && !hasInlineRoll) ){
        return;
    }
   
    let roll = -1;
    
    roll = chatMessage.roll;
    let userRoll = chatMessage.alias;
    
    console.log(roll.terms);

    let numDice = roll.terms[0]["number"];
    let faceDice = roll.terms[0]["faces"];
    let plusMinus = roll.terms[1];
    let bonusDice = roll.terms[2];
    let urlTalespire;
    
    if(plusMinus!=undefined){
        urlTalespire = "talespire://dice/" + userRoll + ":" + numDice + "d" + faceDice + plusMinus + bonusDice;
    }else{
        urlTalespire = "talespire://dice/" + userRoll + ":" + numDice + "d" + faceDice;
    }

    console.log(numDice);
    console.log(faceDice);
    console.log(bonusDice);
    console.log(urlTalespire);

    window.open(urlTalespire,"_blank")


});

