# Install

1. Go to the setup page and choose **Add-on Modules**.
2. Click the **Install Module** button, and paste in this manifest link:
    * https://gitlab.com/ut.ff3d/foundry-talespire/-/raw/master/module.json

- Finally, enable the module on the module settings form under Help.

Compatible with FoundryVTT 0.7.9

# Description

This module turns all Inline rolls from the chat message into a roll in talespire software.

![Screenshot FoundryVTT](https://i.ibb.co/Qb6rXj2/Capture1.jpg) ![Screenshot roll dice](https://i.ibb.co/m0JLjnx/Capture2.jpg)
![Screenshot result](https://i.ibb.co/74tfFh0/Capture3.jpg)

Please launch talespire before.

# TODO

*Find a way to display the skill, weapon, ... the purpose of the dice roll<br>
*Maybe disable the roll message in foundryVTT, as the dice results will be different

> **NOTE:** First time you to check the pop-up message asking to always launch talespire external software.

